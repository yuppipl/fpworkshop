package example

import example.Hello.Foo
import spire.algebra._
import spire.implicits._

object Hello extends App {

  def add1(l: Int, r: Int): Int = l + r
  def add2(l: Double, r: Double): Double = l + r
  def add[A: AdditiveSemigroup](l: A, r: A): A = AdditiveSemigroup[A].plus(l, r)

  println(add(1, 1))
  println(add1(1, 2))
  println(add(1.2, 1.4))
  println(add2(1.1, 2.1))

  case class Foo(age: Int, name: String)

  implicit val fooAdditiveSemigroup: AdditiveSemigroup[Foo] =
    (x: Foo, y: Foo) => Foo(x.age + y.age, x.name + y.name)

  println(add(Foo(2, "Aaa"), Foo(3, "BBB")))

  println(Foo(2, "Aaa") + Foo(3, "BBB"))

  import Fp._
  def print[A : ToString](toText: A): Unit = println(ToString[A].show(toText))
  def print2[A](toText: A)(implicit toString: ToString[A]): Unit = println(toString.show(toText))

  def printLikeInldJava(obj: Object) = println(obj.toString)
  case class MyString(a : String) {
    override def toString: String = s"s(${super.toString})"
  }
  case class MyInt(a : Int) {
    override def toString: String = s"i(${super.toString})"
  }
  printLikeInldJava(MyString("fdsafsd"))



  print2("aaa")
  print(Foo(2, "Aaa") + Foo(3, "BBB"))
  print((2.3, Foo(2, "Aaa")))
}

object Fp {

  trait ToString[A] {
    def show(a: A): String
  }

  //homework
  trait Eq[A] {
    def eq(l: A, r: A): Boolean
  }

  object ToString {
    //def apply[A](implicit a : ToString[A]) = a
    def apply[A : ToString]:ToString[A] = implicitly[ToString[A]]
  }

  implicit val printInt: ToString[Int] = (number: Int) => s"i(${number.toString})"
  implicit val printDouble: ToString[Double] = (number: Double) => s"d(${number.toString})"
  implicit val printString: ToString[String] = (text: String) => s"s(${text})"
  implicit def printFoo(implicit intPrint: ToString[Int], stringPrint: ToString[String]): ToString[Foo] = (foo: Foo) => s"f(${stringPrint.show(foo.name)}, ${intPrint.show(foo.age)})"
  implicit def tuple[A : ToString, B : ToString]: ToString[(A, B)] = (t: (A, B)) => s"t(${ToString[A].show(t._1)}, ${ToString[B].show(t._2)})"

}
